##Getting Started
###installation
 Everything you need to install to get started:
* Home brew 
* Ruby
* jekyll 

#####Quick Start instructions for homebrew and ruby installation

* Step 1: You may already have Ruby installed on your computer. You can check inside a terminal emulator by typing:
```
ruby -v
```
* Step 2: Install a package manager
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
This command is going to install Homebrew for us. 
```
brew install ruby
```
* Step 3: Use the package manager to install Ruby


#####Quick Start instructions for jekyll installation
 
* Step 1: Install a full Ruby development environment
* Step 2: Install Jekyll and bundler gems
``` 
 gem install bundler jekyll
```
* Step 3: Create a new Jekyll site at ./myphotos
```
 jekyll new myphotos
 ```
* Step 4: Change into your new directory
```
 cd myphotos
```
* Step 5: Build the site and make it available on a local server
``` 
 bundle exec jekyll serve
```
* Step 6: Now browse to http://localhost:4000

#####Commands for compressed images
```
brew install imagemagick

chmod +x compress-new-images.sh

chmod +x ./_preplugins/image-compress.sh

./compress-new-images.sh
```

##Introduction
When we created this theme. We are focused on gallery & portfolio modules so you can show your works uniquely and require simple steps to created with our predefined templates and content builder.

## How to create your first post
After the installation, here come the setting up your photos and work. For that, you have the add/ edit the given sample files. 
* Step 1: Go the folder named  _photostories. 
* Step 2: Setting up folder structure for your project.

> go to _photostories > 2018 > 06 > India > india.md 

 In _photostories, you can see folder named as years example : 2018 , 2019. You can change the year according to your need . Example :  you wan to add your project named 'Africa' dated 09-01-2019. then your folder should named as 2019. And in folder 2019 , add another folder named as 01 (month of your work). After that, in 01 folder add another folder Africa (your project name)    

```
If your project named Africa dated 01-12-2019 then your folder structure should be like this

 _photostories > 2019 > 12 > Africa > Africa.md 
 
 where 2019 is year of your project,
       12 is month
       Africa is title / name of your project
```
 * Step 3: Open india.md and you see the following at the top . 
 
 ```
 layout: collection
 title:  "India"
 subtitle: "Summer & Winter"
 date:   2018-06-29 10:42:18 +0530
 cover: "/images/home/greece.jpg"
 months: "MAR & SEPT"
 photos: 1238
 author: Zencat
 
 ```
 
 ![](./images/screenshots/postlayout.png)
 
 Change this code of line according to your needs. Example for your project Africa is given below 
 
 ```
  layout: collection
  title:  "Africa"       //  change the title to your project name 
  subtitle: "Winter"          // change as per need or you can leave it blank
  date:   2019-12-01 10:42:18 +0530       // project date and time 
  cover: ""/images/home/africa.jpg""        // image url 
  months: "NOV & DEC"               //  time period 
  photos: 230                     //   number of photos taken in this location
  author: Zencat                 // author name if any 
 ```
 this changing will be reflecting on your site as given below:
 
 ![](./images/screenshots/postlayout1.png)
 
 * Step 4 : Adding heading to your site 
 
 ```
 ## Winter
 ##### October 2018
 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque aliquam vestibulum morbi blandit cursus risus at. Viverra nibh cras pulvinar mattis nunc sed blandit libero.
 ```

 ![](./images/screenshots/heading.png)
 ```
  ## My Heading
  ##### My sub title
  Description goes there
 ```
  this changing will be reflecting on your site as given below:
  
  ![](./images/screenshots/heading1.png)
  

* Step 5: Adding photos to your site:  

1. First of all, go to _photostories > 2019 > 12 > Africa and add your project photos here.
2. After that, go to _photostories > 2019 > 12 > Africa > africa.md and edit the image address as given below:

```
![](img1.jpg){:class="image"} 
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
![](img2.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
```
Simply, change the address of image as given below. For example : photos added by you are named as myImage1.jpg, myImage2.jpg and myImage3.jpg 
```
![](myImage1.jpg){:class="image"} 
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
![](myImage1.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
![](myImage3.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
```
you can add as many photos as you wish. simply, copy and paste the above code and change the image url.
```
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
```
the above line is the caption of the photo. You can add and remove this line of code as per your need.

Image with caption
```
![](myImage1.jpg){:class="image"} 
###### Photo Caption 
```
Image without caption
```
![](myImage1.jpg){:class="image"} 
```
* Step 8: Adding number of photos per row 
    * adding single photo per row :
    
    For adding single photo in a row then add your photo under the html tag 'div' with 'class' = image-container as given below:
    ```
    <div class="image-container">
   
    ![](img1.jpg){:class="image"}
    ###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
    ![](photoset1.jpg){:class="image"}
    ###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
    ![](photoset2.jpg){:class="image"}
    
    </div>
    ```
    * adding two photos per row :
     For adding two photos in a row. see code as given below:
    ```
    <div class="image-row-2">
    <div class="caption-container">
    
    ![](photoset11.jpg){:class="image"}  // add your inage url here
    ###### photo Caption for image 1    // optional
    </div>
    <div class="caption-container">
    
    ![](photoset10.jpg){:class="image"}   // add your inage url here
    ###### photo Caption for image 2   // optional
    </div>
    </div>
    ```
    * adding four photos per row :
         For adding four photos in a row.then add your photo under the html tag 'div' with 'class' = image-row-4 as given below:
    ```
    <div class="image-row-4">
    
    ![](photoset11.jpg){:class="image"}
    ![](photoset10.jpg){:class="image"}
    ![](photoset16.jpg){:class="image"}
    ![](photoset14.jpg){:class="image"}
    
    </div>
    ```
    > NOTE : You can't add photo caption in four photos in a row section
* Step 7: Adding Fun fact section to your post .

![](./images/screenshots/funfact.png)

Edit the following as per need.
```
<div class="fun-fact">

#### FUN FACT // heading
![](flag.jpg){:class="flag-image"}  // image url
Lorem ipsum dolor sit eget felis eget. Natoque. // description in fun fact part goes here
 
</div>
```

>NOTE: If You dont need this fun fact part in your project, just remove the above lines of code from your africe.md file.

* Step 8: Heading Syntax
  * To use location icon in heading. Use this code of line:
  ![](./images/screenshots/locationicon.png)
  ```
   ### India
  ```
  * To use simple heading. Use this code of line:
   ![](./images/screenshots/heading.png)
  ```
   ## Wnter
  ```
 
Now, your post is ready.

## Adding Social links to your site 

> go to _config.yml and edit the following:

```
twitter_username: StudiosZencat  // changed to twitter_username: abc  
github_username:  zencatstudios  // in place of zencatstudios , add your username
dribbble_username: ZencatStudio
linkedin_username: zencat-studios
copy_right: https://zencat.studio/ 
```






