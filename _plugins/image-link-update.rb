=begin 
    """
    # aklsdjfajdfk
    ## asdkjlfhlakdjsf
    ![asdf](https://asdfljasd.coamsdflj.calsdkjf/aslfdk)
    ![asdf](nu_coach.png)
    """

    IN THE ABOVE STRING THIS FUNCTION WILL PREPEND THE PREPEND STRING TO IMAGE_URLS WITHOUT HTTP IN THEM, LIKE:

    """
    # aklsdjfajdfk
    ## asdkjlfhlakdjsf
    ![asdf](https://asdfljasd.coamsdflj.calsdkjf/aslfdk)
    ![asdf](/2009/12/02/nu_coach.png)
    """
=end
def replace_image_link(prepend, content)
    regex = /\!\[.*\]\((.*)\)/
    results = content.to_enum(:scan, regex).map { Regexp.last_match }
    dest_string = content
    set = Set.new

    for match in results
        if !match[1].start_with?('http')
            if set.add?(match[1])
                # if an image is used multiple times in a post
                # this condition will help just replace it once
                # as gsub will replace all instances in dest_string
                dest_string = dest_string.gsub(match[1], prepend+"/"+match[1].split(".").join(".c."))
            end
        end
    end
    dest_string
end

def removeSlashes(s)
    x = s.sub(/(\/)+$/,'') # remove from end
    x = s.sub(/^(\/)+/,'') # remove from beginning
    x
end

Jekyll::Hooks.register :documents, :pre_render do |document|
    collection_name = document.collection.label
    permalink = document.site.config["collections"][collection_name]["permalink"]
    year = document.date.year.to_s
    month = document.date.month.to_s.rjust(2, "0")
    day = document.date.day.to_s.rjust(2, "0")
    title = document.slug

    uri_elements = URI(document.path).path.split('/')
    object_name = uri_elements.pop
    object_dir = uri_elements.join('/').sub! document.site.source+"/",""
    object_dir = object_dir.sub! "_"+collection_name, collection_name

    permalink = permalink
        .sub(":year", year)
        .sub(":month", month)
        .sub(":day", day)
        .sub(":title", title)
        .sub(":path", object_dir)
    base_url = ENV["BASE_URL"].to_s
    sub_uri = ENV["PHOTO_ROOT"].to_s
    link = base_url + sub_uri + permalink
    document.content = replace_image_link(link, document.content)

    document
end
  