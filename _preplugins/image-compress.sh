#!/bin/bash

cd "$(dirname "$1")"
filename=$(basename -- "$1")
extension="${filename##*.}"
filename="${filename%.*}"
/usr/local/bin/convert "$1" -strip -resize 2000x2000 -quality 25% "$filename".f."$extension"
/usr/local/bin/convert "$1" -strip -resize 500x500 -blur 0x8 -quality 50% "$filename".c."$extension"

#chmod +x /path/to/yourscript.sh